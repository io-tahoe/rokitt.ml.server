FROM openjdk:8-jdk-slim
#add will unpack artifact
ADD target/open-ml-bin.tar.gz /opt
EXPOSE 8090
VOLUME [ "/opt/open-ml/log" ]
CMD [ "java", "-XX:+UnlockExperimentalVMOptions", "-XX:+UseCGroupMemoryLimitForHeap","-jar","/opt/open-ml/open-ml.jar" ]