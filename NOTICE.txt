Copyright (c) 2017 RokiTT Astra

Included Third Party Software:
********************************************************************************
Spring Boot 1.3.8.RELEASE
License: Apache License 2.0 (https://www.apache.org/licenses/LICENSE-2.0)
********************************************************************************
Spring Boot Validation Starter 1.3.8.RELEASE
License: Apache License 2.0 (https://www.apache.org/licenses/LICENSE-2.0)
********************************************************************************
SnakeYAML 1.16
License: Apache License 2.0 (https://www.apache.org/licenses/LICENSE-2.0)
********************************************************************************
Spring Expression Language (SpEL) 4.2.8.RELEASE
License: Apache License 2.0 (https://www.apache.org/licenses/LICENSE-2.0)
********************************************************************************
Spring Framework 4.2.8.RELEASE
License: Apache License 2.0 (https://www.apache.org/licenses/LICENSE-2.0)
********************************************************************************
Spring Plugin Core 1.2.0.RELEASE
License: Apache License 2.0 (https://www.apache.org/licenses/LICENSE-2.0)
********************************************************************************
********************************************************************************
tomcat-embed-core 8.0.37
License: Apache License 2.0 (https://www.apache.org/licenses/LICENSE-2.0)
********************************************************************************
tomcat-embed-el 8.0.37
License: Apache License 2.0 (https://www.apache.org/licenses/LICENSE-2.0)
********************************************************************************
tomcat-embed-websocket 8.0.37
License: Apache License 2.0 (https://www.apache.org/licenses/LICENSE-2.0)
********************************************************************************
Hibernate Validator 5.2.4.Final
License: Apache License 2.0 (https://www.apache.org/licenses/LICENSE-2.0)
********************************************************************************
JBoss Logging 3.2.1.Final
License: Apache License 2.0 (https://www.apache.org/licenses/LICENSE-2.0)
********************************************************************************
Jackson JSON processor 2.6.7
License: Apache License 2.0 (https://www.apache.org/licenses/LICENSE-2.0)
********************************************************************************
AOP Alliance (Java/J2EE AOP standard) 1.0
License: Public Domain
********************************************************************************
jcl104-over-slf4j 1.7.21
License: Apache License 2.0 (https://www.apache.org/licenses/LICENSE-2.0)
********************************************************************************
JUL to SLF4J bridge 1.7.21
License: MIT License (https://opensource.org/licenses/MIT)
********************************************************************************
Log4j Implemented Over SLF4J 1.7.21
License: Apache License 2.0 (https://www.apache.org/licenses/LICENSE-2.0)
********************************************************************************
log4j 1.2.17
License: Apache License 2.0 (https://www.apache.org/licenses/LICENSE-2.0)
********************************************************************************
java-classmate 1.3.1
License: Apache License 2.0 (https://www.apache.org/licenses/LICENSE-2.0)
********************************************************************************
SLF4J API Module 1.7.13
License: MIT License (https://www.slf4j.org/license.html)
Notice:

 Copyright (c) 2004-2017 QOS.ch
 All rights reserved.

 Permission is hereby granted, free  of charge, to any person obtaining
 a  copy  of this  software  and  associated  documentation files  (the
 "Software"), to  deal in  the Software without  restriction, including
 without limitation  the rights to  use, copy, modify,  merge, publish,
 distribute,  sublicense, and/or sell  copies of  the Software,  and to
 permit persons to whom the Software  is furnished to do so, subject to
 the following conditions:

 The  above  copyright  notice  and  this permission  notice  shall  be
 included in all copies or substantial portions of the Software.

 THE  SOFTWARE IS  PROVIDED  "AS  IS", WITHOUT  WARRANTY  OF ANY  KIND,
 EXPRESS OR  IMPLIED, INCLUDING  BUT NOT LIMITED  TO THE  WARRANTIES OF
 MERCHANTABILITY,    FITNESS    FOR    A   PARTICULAR    PURPOSE    AND
 NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 OF CONTRACT, TORT OR OTHERWISE,  ARISING FROM, OUT OF OR IN CONNECTION
 WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
********************************************************************************
Java CUP Parser Generator 0.11a
License: CUP Parser Generator Copyright Notice, License and Disclaimer
Notice:
Copyright 1996-2015 by Scott Hudson, Frank Flannery, C. Scott Ananian, Michael Petter
Permission to use, copy, modify, and distribute this software and its documentation for any purpose and without fee is hereby granted, provided that the above copyright notice appear in all copies and that both the copyright notice and this permission notice and warranty disclaimer appear in supporting documentation, and that the names of the authors or their employers not be used in advertising or publicity pertaining to distribution of the software without specific, written prior permission.

The authors and their employers disclaim all warranties with regard to this software, including all implied warranties of merchantability and fitness. In no event shall the authors or their employers be liable for any special, indirect or consequential damages or any damages whatsoever resulting from loss of use, data or profits, whether in an action of contract, negligence or other tortious action, arising out of or in connection with the use or performance of this software.
********************************************************************************
Matrix Toolkits for Java 1.0.4 API
License: GNU Lesser General Public License v2.1 or later (https://www.gnu.org/licenses/old-licenses/lgpl-2.1.en.html) (https://www.gnu.org/licenses/old-licenses/lgpl-2.1.en.html)
Notice:
You are receiving a copy of the library in the following [Installation Dir]/lib/external folder. The terms of the Rokkit Astra license do NOT apply to that library. If you do not wish to install this library, you may remove the files after installation, but the Astra program might not operate properly or at all without that library.
********************************************************************************
Netlib Java 1.1 (http://www.netlib.org/) (https://github.com/fommil/netlib-java)
License: BSD 3-clause "New" or "Revised" License (https://opensource.org/licenses/BSD-3-Clause)
Notice:
Copyright (c) 2013 Samuel Halliday
Copyright (c) 1992-2011 The University of Tennessee and The University
                        of Tennessee Research Foundation.  All rights
                        reserved.
Copyright (c) 2000-2011 The University of California Berkeley. All
                        rights reserved.
Copyright (c) 2006-2011 The University of Colorado Denver.  All rights
                        reserved.
********************************************************************************
JniLoader 1.1
License: GNU Lesser General Public License v2.1 or later (https://www.gnu.org/licenses/old-licenses/lgpl-2.1.en.html) (https://www.gnu.org/licenses/old-licenses/lgpl-2.1.en.html)
Notice:
You are receiving a copy of the library in the following [Installation Dir]/lib/external folder. The terms of the Rokkit Astra license do NOT apply to that library. If you do not wish to install this library, you may remove the files after installation, but the Astra program might not operate properly or at all without that library.
********************************************************************************
Fortran To Java ARPACK 0.1 (http://icl.cs.utk.edu/f2j/)
License: BSD 3-clause "New" or "Revised" License (https://opensource.org/licenses/BSD-3-Clause)
********************************************************************************
Weka Stable 3.8.1 (http://www.cms.waikato.ac.nz/ml/weka/)
License: GNU General Public License 3 (https://www.gnu.org/licenses/gpl-3.0.txt)
Notice:
You are receiving a copy of the library in the following [Installation Dir]/lib/external folder. The terms of the Rokkit Astra license do NOT apply to that library. If you do not wish to install this library, you may remove the files after installation, but the Astra program might not operate properly or at all without that library.
********************************************************************************


