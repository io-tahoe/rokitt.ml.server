#!/bin/bash

APP_HOME="$( cd "$( dirname "${BASH_SOURCE[0]}" )/.." && pwd )"
PATH_TO_JAR=$APP_HOME/open-ml.jar
PID_FILE=$APP_HOME/tmp/open-ml.jar.pid
AS_ENV_FILE=$APP_HOME/open-ml-env

#load env file
if [ -f "$AS_ENV_FILE" ]; then
    . "$AS_ENV_FILE"
fi

START_SCRIPT="nohup java -jar $OPEN_ML_JAVA_OPT  -Dserver.port=$OPEN_ML_SERVER_PORT $PATH_TO_JAR "

# ***********************************************
# ***********************************************


start() {
  PID=`$START_SCRIPT > /dev/null 2>&1 & echo $!`
}

case "$1" in
start)
    if [ -f $PID_FILE ]; then
        PID=`cat $PID_FILE`
        if [ -z "`ps aux | grep ${PID} | grep -v grep`" ]; then
            start
        else
            echo  "Already running open-ml [$PID]"
            exit 0
        fi
    else
        start
    fi

    if [ -z $PID ]; then
        echo "Failed starting open-ml"
        exit 3
    else
        echo $PID > $PID_FILE
        echo "Started open-ml [$PID]"
        exit 0
    fi
;;

status)
    if [ -f $PID_FILE ]; then
        PID=`cat $PID_FILE`
        if [ -z "`ps aux | grep ${PID} | grep -v grep`" ]; then
            echo "Not running (process dead but pidfile exists)"
            exit 1
        else
            echo "Running open-ml [$PID]"
            exit 0
        fi
    else
        echo "Not running open-ml"
        exit 3
    fi
;;

stop)
    if [ -f $PID_FILE ]; then
        PID=`cat $PID_FILE`
        if [ -z "`ps aux | grep ${PID} | grep -v grep`" ]; then
            echo "Not running (process dead but pidfile exists)"
            rm -f $PID_FILE
            exit 1
        else
            PID=`cat $PID_FILE`
            kill -9 $PID
            rm -f $PID_FILE
            echo "Stopped open-ml [$PID]"
            exit 0
        fi
    else
        echo $PID_FILE
        echo "Not running open-ml (pid not found)"
        exit 3
    fi
;;

restart)
    $0 stop
    sleep 5
    $0 start
;;

*)
    echo "Usage: $0 {status|start|stop|restart}"
    exit 1
esac