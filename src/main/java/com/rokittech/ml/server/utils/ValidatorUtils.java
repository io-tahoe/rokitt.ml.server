/*
 * Copyright 2017 ROKITT Inc.
 * (https://www.rokittastra.com)

 * This program is a free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.

 * This program also uses Spring software that is licensed under
 * the Apache License, Version 2.0 (the "License");
 * you may not use Spring files except in compliance with the License.
 *
 * We are using Spring software with Apache 2 license according to the
 * recommendations of ASF:
 *
 *      https://www.apache.org/licenses/GPL-compatibility.html
 *
 * You may obtain a copy of the Apache 2 License at

 *      http://www.apache.org/licenses/LICENSE-2.0

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License and Apache 2 License for more details.
 *
 */
package com.rokittech.ml.server.utils;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import java.util.Collection;
import java.util.Map;

/**
 * Created by andrii.zavalnyi on 3/11/17.
 */
public class ValidatorUtils {
    private static Logger logger = LogManager.getLogger(ValidatorUtils.class);

    public static void notNull(Object o, String msg) {
        if (o == null) {
            logger.error(msg);
            throw new IllegalArgumentException(msg);
        }
    }

    public static void notNull(Object o) {
        notNull(o, "Object can not be null");
    }

    public static void notEmpty(Collection<?> iterable, String msg) {
        if (iterable == null || iterable.isEmpty()) {
            logger.error(msg);
            throw new IllegalArgumentException(msg);
        }
    }

    public static void notEmpty(Collection<?> iterable) {
        notEmpty(iterable, "Collection can not be empty");
    }

    public static void notEmpty(Map<?, ?> iterable, String msg) {
        if (iterable == null || iterable.isEmpty()) {
            logger.error(msg);
            throw new IllegalArgumentException(msg);
        }
    }

    public static void notEmpty(Map<?, ?> iterable) {
        notEmpty(iterable, "Collection can not be empty");
    }

    public static void notEmpty(String o, String msg) {
        notNull(o);
        if (o.isEmpty()) {
            logger.error(msg);
            throw new IllegalArgumentException(msg);
        }
    }

    public static void notEmpty(String o) {
        notEmpty(o, "String can not be empty");
    }

}