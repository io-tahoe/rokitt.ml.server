/*
 * Copyright 2017 ROKITT Inc.
 * (https://www.rokittastra.com)

 * This program is a free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.

 * This program also uses Spring software that is licensed under
 * the Apache License, Version 2.0 (the "License");
 * you may not use Spring files except in compliance with the License.
 *
 * We are using Spring software with Apache 2 license according to the
 * recommendations of ASF:
 *
 *      https://www.apache.org/licenses/GPL-compatibility.html
 *
 * You may obtain a copy of the Apache 2 License at

 *      http://www.apache.org/licenses/LICENSE-2.0

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License and Apache 2 License for more details.
 *
 */
package com.rokittech.ml.server.endpoint;

import com.rokittech.ml.server.dto.*;
import com.rokittech.ml.server.services.MLService;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

/**
 * Created by andrii.zavalnyi on 3/11/17.
 */
@RestController
@RequestMapping("/ml")
public class MLRestController {

    @Resource
    private MLService mlService;

    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(value = "/validateModel", method = RequestMethod.POST)
    public ValidateModelResponse validateTestModel(@RequestBody ValidateModelRequest request) {
        return mlService.validateTestModel(request);
    }

    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(value = "/classifyModel",method = RequestMethod.POST)
    public ClassifyModelsResponse classifyModels(@RequestBody ClassifyModelsRequest request) {
        return mlService.classifyModels(request);
    }
}
