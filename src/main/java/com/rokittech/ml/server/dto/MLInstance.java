/*
 * Copyright 2017 ROKITT Inc.
 * (https://www.rokittastra.com)

 * This program is a free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.

 * This program also uses Spring software that is licensed under
 * the Apache License, Version 2.0 (the "License");
 * you may not use Spring files except in compliance with the License.
 *
 * We are using Spring software with Apache 2 license according to the
 * recommendations of ASF:
 *
 *      https://www.apache.org/licenses/GPL-compatibility.html
 *
 * You may obtain a copy of the Apache 2 License at

 *      http://www.apache.org/licenses/LICENSE-2.0

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License and Apache 2 License for more details.
 *
 */
package com.rokittech.ml.server.dto;

/**
 * Created by andrii.zavalnyi on 3/11/17.
 */
public class MLInstance {

    private Long mlInstanceIndex;
    private Long index;
    private Double f1;
    private Double f2;
    private Double f3;
    private Double f4;
    private Double f5;
    private Double f6;
    private Double f7;
    private Double f8;
    private Double f9;
    private Double f10;
    private Double f11;
    private Double f12;
    private Double f13;
    private Double f14;
    private Boolean isReal;

    public Double getF1() {
        return f1;
    }

    public void setF1(Double f1) {
        this.f1 = f1;
    }

    public Double getF2() {
        return f2;
    }

    public void setF2(Double f2) {
        this.f2 = f2;
    }

    public Double getF3() {
        return f3;
    }

    public void setF3(Double f3) {
        this.f3 = f3;
    }

    public Double getF4() {
        return f4;
    }

    public void setF4(Double f4) {
        this.f4 = f4;
    }

    public Double getF5() {
        return f5;
    }

    public void setF5(Double f5) {
        this.f5 = f5;
    }

    public Double getF6() {
        return f6;
    }

    public void setF6(Double f6) {
        this.f6 = f6;
    }

    public Double getF7() {
        return f7;
    }

    public void setF7(Double f7) {
        this.f7 = f7;
    }

    public Double getF8() {
        return f8;
    }

    public void setF8(Double f8) {
        this.f8 = f8;
    }

    public Double getF9() {
        return f9;
    }

    public void setF9(Double f9) {
        this.f9 = f9;
    }

    public Double getF10() {
        return f10;
    }

    public void setF10(Double f10) {
        this.f10 = f10;
    }

    public Double getF11() {
        return f11;
    }

    public void setF11(Double f11) {
        this.f11 = f11;
    }

    public Double getF12() {
        return f12;
    }

    public void setF12(Double f12) {
        this.f12 = f12;
    }

    public Double getF13() {
        return f13;
    }

    public void setF13(Double f13) {
        this.f13 = f13;
    }

    public Double getF14() {
        return f14;
    }

    public void setF14(Double f14) {
        this.f14 = f14;
    }

    public Boolean getIsReal() {
        return isReal;
    }

    public void setIsReal(Boolean isReal) {
        this.isReal = isReal;
    }

    public Long getMlInstanceIndex() {
        return mlInstanceIndex;
    }

    public void setMlInstanceIndex(Long mlInstanceIndex) {
        this.mlInstanceIndex = mlInstanceIndex;
    }

    public Long getIndex() {
        return index;
    }

    public void setIndex(Long index) {
        this.index = index;
    }
}
