package com.rokittech.ml.server.services.impl;

import com.rokittech.ml.server.dto.*;
import com.rokittech.ml.server.exceptions.InternalException;
import com.rokittech.ml.server.services.MLService;
import com.rokittech.ml.server.utils.MLUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import weka.classifiers.Classifier;
import weka.classifiers.Evaluation;
import weka.core.Attribute;
import weka.core.Instance;
import weka.core.Instances;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

import static com.rokittech.ml.server.utils.ValidatorUtils.notEmpty;
import static com.rokittech.ml.server.utils.ValidatorUtils.notNull;

/**
 * Created by andrii.zavalnyi on 3/11/17.
 */

@Service
public class MLServiceImpl implements MLService {
    private static final Logger logger = LoggerFactory.getLogger(MLServiceImpl.class);

    private Instances buildHeader(List<String> features) {
        ArrayList attributes = new ArrayList(features.size());
        attributes.addAll(features.stream().map(Attribute::new).collect(Collectors.toList()));

        ArrayList resultLabels = new ArrayList();
        resultLabels.add("Y");
        resultLabels.add("N");
        attributes.add(new Attribute("Flag", resultLabels));
        Instances header = new Instances("header", attributes, attributes.size());
        header.setClassIndex(header.numAttributes() - 1);
        return header;
    }

    @Override
    public ValidateModelResponse validateTestModel(ValidateModelRequest request) {
        try (ByteArrayOutputStream bos = new ByteArrayOutputStream();
             ObjectOutput out = new ObjectOutputStream(bos)) {
            notEmpty(request.getMlFeatures());
            notEmpty(request.getMlAlgorithm());
            notEmpty(request.getMlInstances());

            logger.info("Start validating ML model");
            Instances header = buildHeader(request.getMlFeatures());
            Instances instances = new Instances(header);
            instances.addAll(
                request.getMlInstances().stream()
                    .map(mlInstance -> MLUtils.toTesInstance(mlInstance, request.getMlFeatures()))
                    .collect(Collectors.toList()));

            instances.setClassIndex(instances.numAttributes() - 1);

            Classifier classifier = MLUtils.getClassifier(request.getMlAlgorithm());
            classifier.buildClassifier(instances);

            Evaluation eval = new Evaluation(instances);
            eval.crossValidateModel(classifier, instances, 10, new Random(1));

            out.writeObject(classifier);
            out.flush();

            ValidateModelResponse response = new ValidateModelResponse();
            response.setModel(bos.toByteArray());
            logger.info("Done validating ML model");
            return response;
        } catch (Throwable e) {
            logger.error(e.getMessage(), e);
            throw new InternalException(e.getMessage(), e);
        }
    }

    @Override
    public ClassifyModelsResponse classifyModels(ClassifyModelsRequest request) {
        try {
            notNull(request.getMlInstance());
            notNull(request.getModel());
            notEmpty(request.getMlAlgorithm());
            notEmpty(request.getMlFeatures());

            try (ByteArrayInputStream bis = new ByteArrayInputStream(request.getModel());
                 ObjectInput in = new ObjectInputStream(bis)) {
                logger.info("Start classify ML model");
                Classifier classifier = MLUtils.castClassifier(in.readObject(), request.getMlAlgorithm());

                Instances header = buildHeader(request.getMlFeatures());
                Instance inst = MLUtils.toInstance(request.getMlInstance(), request.getMlFeatures());
                inst.setDataset(header);

                ClassifyModelsResponse classifyModelsResponse = new ClassifyModelsResponse();
                classifyModelsResponse.setResult(classifier.classifyInstance(inst));

                logger.info("Finish classify ML model");
                return classifyModelsResponse;
            }
        } catch (Throwable e) {
            logger.error(e.getMessage(), e);
            throw new InternalException(e.getMessage(), e);
        }
    }

}
