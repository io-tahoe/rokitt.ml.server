milestone()
lock(resource: "${env.JOB_NAME}", inversePrecedence: true) {
  node('java') {
    try{
      stage ('Checkout'){
        checkout scm
        gitCommit = sh(returnStdout: true, script: 'git rev-parse HEAD').trim()
        shortCommit = gitCommit.take(6)
        gitBranch = scm.branches[0].name
        dockerTag = gitBranch.replace('\\','.').replace('/','.').take(128)
        buildVersion = "0.0.1"
        buildRelease = new Date().format('yyMMddHHmm')
        buildDisplayName = "${buildVersion}-${buildRelease}(${shortCommit})"
        currentBuild.displayName = buildDisplayName

        println gitCommit
        println shortCommit
        println gitBranch
        println buildVersion
        sh "env"
      }//stage Checkout 
      stage ('Build'){
        milestone()
        //GIT_BRANCH - fix maven git plugin
        //maven.repo.local - isolate maven repo per each job
        withEnv(["GIT_BRANCH=${gitBranch}", "MAVEN_OPTS=-Dmaven.repo.local=${env.HOME}/.m2/repository/${env.JOB_BASE_NAME}"]) {
          //clean and build
          sh "mvn --batch-mode clean package -Pdist-rpm,dist-deb,dist-zip -Ddist.app.version=${buildVersion} -Ddist.app.release=${buildRelease}"
        }
      }//stage Build
      stage ('process build results'){
        //catch up build results
        milestone()
        parallel (
          "upload_artifact_zip": {
            withCredentials([[$class: 'AmazonWebServicesCredentialsBinding', accessKeyVariable: 'AWS_ACCESS_KEY_ID', credentialsId: 'aws-jenkins-astra', secretKeyVariable: 'AWS_SECRET_ACCESS_KEY']]) {
              sh "aws s3 cp target/open-ml-bin.tar.gz s3://astra-repo-eu-central-1/open-ml/${gitBranch}/ --acl public-read --region eu-central-1 --no-progress"
            }
          },
          "upload_artifact_rpm": {
            withCredentials([[$class: 'AmazonWebServicesCredentialsBinding', accessKeyVariable: 'AWS_ACCESS_KEY_ID', credentialsId: 'aws-jenkins-astra', secretKeyVariable: 'AWS_SECRET_ACCESS_KEY']]) {
              sh "aws s3 cp target/rpm/open-ml/RPMS/noarch/open-ml-*.noarch.rpm s3://astra-repo-eu-central-1/open-ml/${gitBranch}/ --acl public-read --region eu-central-1 --no-progress"
            }
          },
          "upload_artifact_deb": {
            withCredentials([[$class: 'AmazonWebServicesCredentialsBinding', accessKeyVariable: 'AWS_ACCESS_KEY_ID', credentialsId: 'aws-jenkins-astra', secretKeyVariable: 'AWS_SECRET_ACCESS_KEY']]) {
              sh "aws s3 cp target/open-ml_*.deb s3://astra-repo-eu-central-1/open-ml/${gitBranch}/ --acl public-read --region eu-central-1 --no-progress"
            }
          }
        )
      }//stage process build results
      docker.withRegistry('https://docker.ua.io-tahoe.com', 'mvn_deploy_user') {
        def serverImageName = "docker.ua.io-tahoe.com/openml:${dockerTag}"
        def serverImage = docker.build("${serverImageName}", "-f Dockerfile .")
        serverImage.push()
      }

      currentBuild.result = 'SUCCESS'
      //send notification if previos build was failed and current is success
      if( currentBuild.previousBuild != null && !hudson.model.Result.SUCCESS.equals(currentBuild.previousBuild.result)) {
        notifyBuildEmail(currentBuild.result)
        notifyMSTeams(currentBuild.result)
      }
    } catch (e) {
      currentBuild.result = 'FAILURE'
      notifyMSTeams(currentBuild.result)
      notifyBuildEmail(currentBuild.result)
      throw e
    } finally {
    }
  }
}//lock

def notifyBuildEmail(String buildStatus) {
  // build status of null means build started
  buildStatus =  buildStatus ?: 'STARTED'

  def subject = "${buildStatus}: Job '${env.JOB_NAME} [${env.BUILD_NUMBER}]'"
  def summary = "${subject} (${env.BUILD_URL})"

  emailext (
    body: summary,
    recipientProviders: [[$class: 'DevelopersRecipientProvider'], [$class: 'RequesterRecipientProvider']],
    subject: subject )
}

def notifyMSTeams(String buildStatus) {
  timeout(1){
    // Default values
    def colorCode = '#FF0000'
    def summary = "${env.JOB_NAME} ${env.BUILD_DISPLAY_NAME}"
    // Override default values based on build status
    if (buildStatus == 'ABORTED') {
      colorCode = '#a0a0a0'
    } else if (buildStatus == 'SUCCESS') {
      colorCode = '#00FF00'
    } else {
      colorCode = '#FF0000'
    }
    // Send notifications
    withCredentials([string(credentialsId: 'ms_team_astra', variable: 'MS_TEAMS_URL')]) {
      office365ConnectorSend (color: colorCode, status: buildStatus, message: summary, webhookUrl: env.MS_TEAMS_URL)
    }
  }
}

